package dev.vkekukh.servlets;

import dev.vkekukh.entity.User;
import dev.vkekukh.entity.UserService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;



@WebServlet(urlPatterns = "/login")
@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String login = req.getParameter("login");
        String pass = req.getParameter("password");
        String msg;
        User user = null;


        if ((user = UserService.checkUser(login, pass)) != null) {

            HttpSession session = req.getSession();
            session.setAttribute("name", user.getName());

            resp.sendRedirect("/questionnaire.html");
        } else {
            resp.sendRedirect("/BadLogin.html");
        }
    }
}
