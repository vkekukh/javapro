package dev.vkekukh.servlets;


import dev.vkekukh.entity.UserService;
import dev.vkekukh.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/registrationCompleted")
public class RegisterUserServlet extends HttpServlet {
    final static String TEMPLATE = getTemplate();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String lastname = req.getParameter("lastname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        User user = new User(name, lastname, email, password);
        String response = registerUser(user);

        resp.getWriter().println(String.format(TEMPLATE, response));
    }

    private static String registerUser(User user) {
        StringBuilder builder = new StringBuilder();

        if (UserService.addUser(user)) {
            builder.append("Welcome ")
                    .append(user.getName())
                    .append("! Registration completed").append("<br>")
                    .append("<a href=\"index.html\" col>Login</a>");

        } else {
            builder.append("Sorry, your email exists in our base....");
        }

        return builder.toString();
    }

    private static String getTemplate() {
        StringBuilder builder = new StringBuilder();
        builder.append("<html>")
                .append("<head>")
                .append("<title>Register</title>")
                .append("</head>")
                .append("<body>")
                .append("<h1>%s</h1>")
                .append("</body>");


        return builder.toString();
    }
}
