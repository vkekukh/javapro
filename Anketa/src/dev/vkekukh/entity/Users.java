package dev.vkekukh.entity;

import java.util.ArrayList;
import java.util.List;

public class Users {

    private List<User> list = new ArrayList<>();

    public Users() {
    }

    public Users(List<User> list) {

        this.list = list;
    }

    public List<User> getList() {
        return list;
    }

    public void setList(List<User> list) {
        this.list = list;
    }
}
