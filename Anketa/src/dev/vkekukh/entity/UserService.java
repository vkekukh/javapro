package dev.vkekukh.entity;


import java.util.ArrayList;
import java.util.List;


public class UserService {

    private static volatile List<User> users = new ArrayList<>();

    public synchronized static Boolean addUser(User user){

        boolean check = users.stream().anyMatch(u -> u.getEmail().equals(user.getEmail()));

        if(!check){
            users.add(user);
            return true;
        }

        return false;
    }

    public static User checkUser(String email, String password){
        User user = null;

        for (User u : users) {
            if(u.getEmail().equalsIgnoreCase(email) && u.getPassword().equals(password)){
                user = u;
                break;
            }
        }
        return user;
    }

    public static User getUserbyEmail(String email){
        User user = null;

        for (User u : users) {
            if(u.getEmail().equalsIgnoreCase(email)){
                user = u;
                break;
            }
        }
        return user;
    }

    public static List<User> getUsers() {
        return users;
    }


    public static void setUsers(List<User> users) {
        UserService.users = users;
    }
}
