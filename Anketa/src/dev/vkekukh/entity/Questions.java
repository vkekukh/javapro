package dev.vkekukh.entity;

import java.util.ArrayList;
import java.util.List;

public class Questions {

    private static volatile List<Question> questions = new ArrayList<>();

    public Questions() {
    }

    public Questions(List<Question> questions) {
        this.questions = questions;
    }

    public static List<Question> getQuestions() {
        return questions;
    }


    @Override
    public String toString() {
        return "Questions{" +
                "questions=" + questions +
                '}';
    }

    public synchronized static void addQuestion(Question question) {
        questions.add(question);
    }


}
