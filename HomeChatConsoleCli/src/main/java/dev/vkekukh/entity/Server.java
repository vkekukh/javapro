package dev.vkekukh.entity;

import dev.vkekukh.api.ServerAPI;
import dev.vkekukh.api.impl.ServerAPIImpl;

public class Server {
    private static boolean successConnection = false;

    private static String host;
    private static String port;
    private static ServerAPI api = ServerAPIImpl.getInstance();


    private Server() {
    }

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        Server.host = host;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        Server.port = port;
    }


    public static String getDescription() {

        StringBuilder builder = new StringBuilder();
        builder.append("Chat address is:")
                .append(host)
                .append(":")
                .append(port);

        return builder.toString();
    }

    public static boolean isSuccessConnection() {
        return successConnection;
    }

    public static void setSuccessConnection(boolean successConnection) {

        Server.successConnection = successConnection;
        api.startGettingMessages();
    }

}
