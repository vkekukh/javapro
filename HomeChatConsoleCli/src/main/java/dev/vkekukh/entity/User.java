package dev.vkekukh.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static dev.vkekukh.utils.Const.DEFAULT_USER;

public class User {

    private static User instanse;

    private String nickName = DEFAULT_USER;
    private String sex;
    private String firstName;
    private String age;
    private String email;
    private String password;

    public static User getInstanse() {
        if (instanse == null) {
            instanse = new User();
        }
        return instanse;
    }

    public static void setInstanse(User instanse) {
        User.instanse = instanse;
    }

    public static String getStaticNickName() {
        return instanse.nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public User setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public User setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getAge() {
        return age;
    }

    public User setAge(String age) {
        this.age = age;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String toJson() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        return gson.toJson(this);
    }

    public static User fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, User.class);
    }

    @Override
    public String toString() {
        return "User{" +
                "nickName='" + nickName + '\'' +
                ", sex='" + sex + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age='" + age + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
