package dev.vkekukh.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import dev.vkekukh.api.ServerAPI;
import dev.vkekukh.api.impl.ServerAPIImpl;
import dev.vkekukh.entity.Message;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static dev.vkekukh.utils.Const.MONITOR_JOB_INTERVAL;

public class MessageReceiver implements Runnable {
    private static int maxMessageId = 0;
    private ServerAPI api = ServerAPIImpl.getInstance();

    @Override
    public void run() {

        while (true) {
            String json = api.getMessages(maxMessageId);
            List<Message> messages = getMessages(json);

            for (Message message : messages) {
                maxMessageId = message.getId();
                Util.showPrefix();
                System.out.println(message.getFrom() + ":" + message.getText());
            }
            try {
                Thread.sleep(MONITOR_JOB_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private List<Message> getMessages(String json) {
        Type listType = new TypeToken<ArrayList<Message>>() {
        }.getType();
        Gson gson = new GsonBuilder().create();
        List<Message> messages = gson.fromJson(json, listType);
        return messages;
    }
}
