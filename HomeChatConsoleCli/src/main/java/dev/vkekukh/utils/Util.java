package dev.vkekukh.utils;

import dev.vkekukh.entity.User;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Util {

    public static void showPrefix() {
        StringBuilder builder = new StringBuilder();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        builder.append("[")
                .append(LocalDateTime.now()
                        .format(formatter))
                .append("] ");

        System.out.print(builder.toString());
    }
}
