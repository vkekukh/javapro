package dev.vkekukh.utils;

import javax.print.DocFlavor;

public class Const {
    public static final String MESSAGE_WELCOME = "Hello! For start input /conf";
    public static final String MESSAGE_QUIT = "See you Later!";
    public static final String MESSAGE_WRONG_COMMAND = "Wrong command!";
    public static final String MESSAGE_SERVER_HOST = "Input server host:";
    public static final String MESSAGE_SERVER_PORT = "Input server port:";
    public static final String MESSAGE_COMMAND_LIST = "Obtain server commands: /commandList";
    public static final String MESSAGE_VALIDATE_SERVER = "Not valid server settings. Use /conf for validate";
    public static final String MESSAGE_USER_NICKNAME = "Input your nickName:";
    public static final String MESSAGE_USER_NAME = "Input your name:";
    public static final String MESSAGE_USER_EMAIL = "Input your email:";
    public static final String MESSAGE_USER_AGE = "Input your age:";
    public static final String MESSAGE_USER_SEX = "Input your sex:";
    public static final String MESSAGE_USER_PASSWORD = "Input your password:";
    public static final String URL_LOGIN = "/login";

    public static final String DEFAULT_USER = "Anonymous";

    public static final char COMMAND_SYMBOL = '/';
    public static final char PRIVATE_SYMBOL = '"';
    public static final char CHATROOM_SYMBOL = '@';

    public static final String COMMAND_QUIT = "/quit";
    public static final String COMMAND_CONF = "/conf";
    public static final String COMMAND_LIST = "/commandList";
    public static final String COMMAND_REGISTER = "/register";
    public static final String COMMAND_ONLINE = "/online";
    public static final String COMMAND_CREATE_CHATROOM = "/createRoom";
    public static final String COMMAND_CHATROOMS = "/chatRooms";
    public static final String COMMAND_JOIN = "/join";
    public static final String COMMAND_GET_USERS = "/getUsers";
    public static final String COMMAND_LOGIN = "/login";
    public static final String COMMAND_LEAVE = "/leave";

    public static final String URL_ADD_MESSAGE = "/addMessage";
    public static final String URL_REGISTER = "/register";
    public static final String URL_COMMAND = "/command";
    public static final String URL_GET_MESSAGES = "/getMessages";

    public static final String ERROR_EMPTY_NICKNAME = "NickName can't be null";
    public static final String ERROR_GET_MESSAGES = "Couldn't get messages";
    public static final String ERROR_WRONG_SETTINGS = "Wrong server settings";
    public static final String ERROR_EMPTY_VALUE = "Empty value";
    public static final String ERROR_ROOM_NOT_FOUND = "Room %s not found";
    public static final String ERROR_EMPTY_ROOM_NAME = "Name of room can't be null";
    public static final String ERROR_DEFAULT_USER_SEND_PRIVATE = "Default user couldn't send private message";
    public static final String ERROR_DEFAULT_USER_ROOMS = "Default user couldn't use chatRooms";
    public static final String ERROR_PRIVATE_TO_DEFAULT = "You can't send message to " + DEFAULT_USER;
    public static final String REGISTER_USER_FAILD = "This nickName already exists";
    public static final String ERROR_USER_OFFLINE = "User %s  is offline!";

    public static final int MONITOR_JOB_INTERVAL = 500;

    public static final String WRONG_USER_NAME = "Wrong user name";
    public static final String WRONG_PASSWORD = "Wrong password";

}
