package dev.vkekukh.handlers;

import dev.vkekukh.api.MessageAPI;
import dev.vkekukh.api.ServerAPI;
import dev.vkekukh.api.impl.MessageAPIImpl;
import dev.vkekukh.api.impl.ServerAPIImpl;
import dev.vkekukh.entity.Message;
import dev.vkekukh.entity.User;
import dev.vkekukh.utils.ConnectionException;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static dev.vkekukh.utils.Const.*;

public class MessageHandler {
    private static Logger logger = Logger.getLogger(MessageHandler.class);
    private static MessageAPI api = new MessageAPIImpl();
    private static ServerAPI serverAPI = ServerAPIImpl.getInstance();

    public static void parseMessage(String str) throws ConnectionException {
        logger.debug("Start parse message");

        Message message = new Message();
        message.setFrom(User.getStaticNickName());
        message.setText(str);
        api.sendMessage(message);

    }

    public static void parsePrivateMessage(String text) throws ConnectionException{
        logger.debug("parse private message");
        String[] strArr = text.split(" ");

        if (User.getStaticNickName().equalsIgnoreCase(DEFAULT_USER)) {
            System.err.println(ERROR_DEFAULT_USER_SEND_PRIVATE);
            return;
        }

        if (strArr.length < 2) {
            parseMessage(text);
            return;
        }

        String userName = strArr[0].substring(1);
        if (!validateUser(userName)) {
            return;
        }

        List<String> wordList = Arrays.stream(strArr).collect(Collectors.toList());
        wordList.remove(0);
        String messageText = wordList.stream().collect(Collectors.joining(" "));
        sendPrivateOrRoomMessage(userName, messageText);
    }

    public static void parseChatRoomMessage(String text) throws ConnectionException {
        logger.debug("parse chat message");

        String[] strArr = text.split(" ");

        if (User.getStaticNickName().equalsIgnoreCase(DEFAULT_USER)) {
            System.err.println(ERROR_DEFAULT_USER_ROOMS);
            return;
        }

        if (strArr.length < 2) {
            parseMessage(text);
            return;
        }

        String roomName = strArr[0].substring(1);
        if (!validateRoom(roomName)) {
            return;
        }

        List<String> wordList = Arrays.stream(strArr).collect(Collectors.toList());
        wordList.remove(0);
        String messageText = wordList.stream().collect(Collectors.joining(" "));
        sendPrivateOrRoomMessage(roomName, messageText);
    }


    private static boolean validateUser(String name) throws ConnectionException{

        if (name.equalsIgnoreCase(DEFAULT_USER)) {
            System.err.println(ERROR_PRIVATE_TO_DEFAULT);
            return false;
        }

        String onlineUsers = serverAPI.getOnlineUsers();
        List<String> users = Arrays.stream(onlineUsers.split("\n"))
                .collect(Collectors.toList());

        if (!users.contains(name)) {
            System.err.println(String.format(ERROR_USER_OFFLINE, name));
            return false;
        }

        return true;
    }

    private static void sendPrivateOrRoomMessage(String user, String text) throws ConnectionException{
        Message message = new Message();
        message.setFrom(User.getStaticNickName());

        message.setTo(user);
        message.setText(text);
        api.sendMessage(message);
    }

    private static boolean validateRoom(String name) throws ConnectionException{
        String rooms = serverAPI.getRooms();
        List<String> roomList = Arrays.stream(rooms.split("\n"))
                .collect(Collectors.toList());
        if (!roomList.contains(name)) {
            System.err.println(String.format(ERROR_ROOM_NOT_FOUND, name));
            return false;
        }
        return true;
    }
}
