package dev.vkekukh.handlers;

import dev.vkekukh.api.CommandAPI;
import dev.vkekukh.api.impl.CommandAPIImpl;
import dev.vkekukh.entity.User;
import dev.vkekukh.utils.ConnectionException;

import static dev.vkekukh.utils.Const.*;

public class CommandHandler {

    private static CommandAPI api = new CommandAPIImpl();

    static void parseCommand(String command) throws ConnectionException {

        switch (getCommand(command)) {
            case COMMAND_CONF:
                api.doConf();
                break;
            case COMMAND_LIST:
                api.doCommandList();
                break;
            case COMMAND_ONLINE:
                api.doOnline();
                break;
            case COMMAND_REGISTER:
                api.doRegister();
                break;
            case COMMAND_CREATE_CHATROOM:
                api.doCreateChatRoom(getValue(command));
                break;
            case COMMAND_CHATROOMS:
                api.doChatRooms();
                break;
            case COMMAND_JOIN:
                api.doJoin(getValue(command));
                break;
            case COMMAND_LEAVE:
                api.doLeave(getValue(command));
                break;
            case COMMAND_GET_USERS:
                api.doGetUsers(getValue(command));
                break;
            case COMMAND_LOGIN:
                api.doLogin();
                break;
            case COMMAND_QUIT:
                System.out.println(MESSAGE_QUIT);
                break;
            default:
                System.out.println(MESSAGE_WRONG_COMMAND);
        }
    }

    private static String getValue(String command) {
        String[] strings = command.split(" ");
        if (strings.length < 2) {
            System.err.println(ERROR_EMPTY_VALUE);
            return null;
        } else {
            return strings[1];
        }
    }

    private static String getCommand(String command) {
        String[] strings = command.split(" ");
        return strings[0];
    }
}