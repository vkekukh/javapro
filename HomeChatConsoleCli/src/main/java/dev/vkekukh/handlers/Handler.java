package dev.vkekukh.handlers;

import dev.vkekukh.utils.ConnectionException;

import static dev.vkekukh.utils.Const.*;

public class Handler {

    public static void parse(String text) throws ConnectionException {
        if (text.length() > 0) {

            char firstSymbol = text.charAt(0);
            switch (firstSymbol) {
                case COMMAND_SYMBOL:
                    CommandHandler.parseCommand(text);
                    break;
                case PRIVATE_SYMBOL:
                    MessageHandler.parsePrivateMessage(text);
                    break;
                case CHATROOM_SYMBOL:
                    MessageHandler.parseChatRoomMessage(text);
                    break;
                default:
                    MessageHandler.parseMessage(text);
            }
        }
    }

}
