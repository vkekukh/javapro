package dev.vkekukh;

import dev.vkekukh.entity.User;
import dev.vkekukh.handlers.Handler;
import dev.vkekukh.utils.ConnectionException;

import java.util.Scanner;

import static dev.vkekukh.utils.Const.*;

public class Main {

    public static void main(String[] args) {
        User user = User.getInstanse();

        System.out.println(MESSAGE_WELCOME);

        String line = "";
        Scanner scanner = new Scanner(System.in);

        while (!line.equalsIgnoreCase(COMMAND_QUIT)) {
            System.out.print(User.getStaticNickName() + ":");
            line = scanner.nextLine().trim();
            if (line.length() > 0) {
                try {
                    Handler.parse(line);
                } catch (ConnectionException e) {
                    System.err.println(MESSAGE_VALIDATE_SERVER);
                }
            }
        }
    }
}
