package dev.vkekukh.api.impl;

import dev.vkekukh.api.MessageAPI;
import dev.vkekukh.api.ServerAPI;
import dev.vkekukh.entity.Message;
import dev.vkekukh.utils.ConnectionException;

public class MessageAPIImpl implements MessageAPI {
    private static ServerAPI api = ServerAPIImpl.getInstance();

    @Override
    public void sendMessage(Message message) throws ConnectionException{
        api.sendMessage(message.toJson());
    }

}
