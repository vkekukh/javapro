package dev.vkekukh.api.impl;

import dev.vkekukh.api.CommandAPI;
import dev.vkekukh.api.ServerAPI;
import dev.vkekukh.entity.Server;
import dev.vkekukh.entity.User;
import dev.vkekukh.utils.ConnectionException;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Scanner;

import static dev.vkekukh.utils.Const.*;

public class CommandAPIImpl implements CommandAPI {
    private static Logger logger = Logger.getLogger(CommandAPIImpl.class);
    private static ServerAPI api = ServerAPIImpl.getInstance();

    @Override
    public void doConf() {
        Scanner scanner = new Scanner(System.in);

        System.out.print(MESSAGE_SERVER_HOST);
        String host = scanner.nextLine();

        if (host.equals("")) {
            host = "localhost";
        }

        System.out.print(MESSAGE_SERVER_PORT);
        String port = scanner.nextLine();
        if (port.equals("")) {
            port = "8080";
        }

        Server.setHost(host);
        Server.setPort(port);

        if (!api.checkConnection()) {
            System.err.println(ERROR_WRONG_SETTINGS);
        } else {
            System.out.println(Server.getDescription());
            System.out.println(MESSAGE_COMMAND_LIST);
        }

    }

    @Override
    public void doCommandList() throws ConnectionException {
        HashMap<String, String> params = new HashMap<>();
        params.put("name", COMMAND_LIST);
        api.sendCommand(params);
    }

    @Override
    public void doRegister() throws ConnectionException {
        Scanner scanner = new Scanner(System.in);

        System.out.print(MESSAGE_USER_NICKNAME);
        String nickName = scanner.nextLine();

        if (nickName.equals("")) {
            System.err.println(ERROR_EMPTY_NICKNAME);
            return;
        }

        System.out.print(MESSAGE_USER_NAME);
        String name = scanner.nextLine();

        System.out.print(MESSAGE_USER_EMAIL);
        String email = scanner.nextLine();

        System.out.print(MESSAGE_USER_AGE);
        String age = scanner.nextLine();

        System.out.print(MESSAGE_USER_SEX);
        String sex = scanner.nextLine();

        System.out.print(MESSAGE_USER_PASSWORD);
        String password = scanner.nextLine();

        User user = User.getInstanse();
        user.setNickName(nickName)
                .setFirstName(name)
                .setEmail(email)
                .setAge(age)
                .setSex(sex)
                .setPassword(password);

        String userJson = user.toJson();
        api.registerUser(userJson);
    }

    @Override
    public void doLogin() throws ConnectionException {
        Scanner scanner = new Scanner(System.in);

        System.out.print(MESSAGE_USER_NICKNAME);
        String nickName = scanner.nextLine();

        System.out.print(MESSAGE_USER_PASSWORD);
        String password = scanner.nextLine();


        api.doLogin(nickName, password);
    }

    @Override
    public void doOnline() throws ConnectionException {
        HashMap<String, String> params = new HashMap<>();
        params.put("name", COMMAND_ONLINE);
        api.sendCommand(params);
    }

    @Override
    public void doCreateChatRoom(String name) throws ConnectionException {
        if (checkDefaultUser()) {
            return;
        } else if (name == null) {
            System.err.println(ERROR_EMPTY_ROOM_NAME);
        } else {
            HashMap<String, String> params = new HashMap<>();
            params.put("name", COMMAND_CREATE_CHATROOM);
            params.put("value", name);

            api.sendCommand(params);
        }
    }

    @Override
    public void doChatRooms() throws ConnectionException {
        if (checkDefaultUser()) {
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("name", COMMAND_CHATROOMS);
        api.sendCommand(params);
    }

    @Override
    public void doJoin(String name) throws ConnectionException {
        if (checkDefaultUser()) {
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("name", COMMAND_JOIN);
        params.put("value", name);
        api.sendCommand(params);
    }

    @Override
    public void doGetUsers(String name) throws ConnectionException {
        HashMap<String, String> params = new HashMap<>();
        params.put("name", COMMAND_GET_USERS);
        params.put("value", name);
        api.sendCommand(params);
    }

    @Override
    public void doLeave(String value) throws ConnectionException {
        if (checkDefaultUser()) {
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("name", COMMAND_LEAVE);
        params.put("value", value);
        api.sendCommand(params);
    }

    private boolean checkDefaultUser() {
        if (User.getStaticNickName().equals(DEFAULT_USER)) {
            System.err.println(ERROR_DEFAULT_USER_ROOMS);
            return true;
        }
        return false;
    }
}
