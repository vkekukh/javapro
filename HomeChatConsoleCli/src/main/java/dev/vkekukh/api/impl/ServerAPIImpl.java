package dev.vkekukh.api.impl;

import dev.vkekukh.api.ServerAPI;
import dev.vkekukh.entity.Server;
import dev.vkekukh.entity.User;
import dev.vkekukh.utils.ConnectionException;
import dev.vkekukh.utils.MessageReceiver;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;


import java.io.*;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static dev.vkekukh.utils.Const.*;

public class ServerAPIImpl implements ServerAPI {
    private static Logger logger = Logger.getLogger(ServerAPIImpl.class);
    private HttpClient http = null;
    private String urlStr;
    private static  ServerAPIImpl instance;

    private ServerAPIImpl() {
        init();
    }

    public static ServerAPIImpl getInstance() {
        if (instance == null){
            instance = new ServerAPIImpl();
        }
        return instance;
    }

    public void init() {
        logger.debug("init");
        CookieStore cookieStore = new BasicCookieStore();
        HttpClientBuilder clientBuilder = HttpClientBuilder.create().setDefaultCookieStore(cookieStore);
        http = clientBuilder.build();

        StringBuilder builder = new StringBuilder();
        builder.append("http://")
                .append(Server.getHost())
                .append(":")
                .append(Server.getPort());
        urlStr = builder.toString();
    }

    public void startGettingMessages() {
        Thread thread = new Thread(new MessageReceiver());
        thread.setDaemon(true);
        thread.start();
    }

    @Override
    public void sendCommand(HashMap<String, String> params) throws ConnectionException {

        checkSuccess();

        StringBuilder builder = new StringBuilder();

        builder.append(urlStr)
                .append(URL_COMMAND)
                .append(getParamString(params));

        HttpGet request = new HttpGet(builder.toString());
        try {
            HttpResponse response = http.execute(request);

            System.out.println(getAnswer(response));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void registerUser(String json) throws ConnectionException {

        checkSuccess();

        logger.debug(urlStr);
        StringBuilder builder = new StringBuilder();
        builder.append(urlStr)
                .append(URL_REGISTER);

        String answer = sendJson(builder.toString(), json);
        System.out.println(answer);
        if (answer.equals(REGISTER_USER_FAILD)) {
            doDefaultUser();
        }
    }

    @Override
    public void doLogin(String login, String password) throws ConnectionException {

        checkSuccess();

        StringBuilder builder = new StringBuilder();
        Map<String, String> params = new HashMap<>();
        params.put("login", login);
        params.put("password", password);

        builder.append(urlStr)
                .append(URL_LOGIN)
                .append(getParamString(params));

        HttpPost request = new HttpPost(builder.toString());

        HttpResponse response = null;

        try {
            response = http.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String responseStr = getAnswer(response);

        if (responseStr.equals(WRONG_USER_NAME) ||
                responseStr.equals(WRONG_PASSWORD)) {
            System.err.println(responseStr);
        } else {

            User user = User.fromJson(responseStr);
            User.setInstanse(user);

            System.out.println(MESSAGE_WELCOME);
        }
    }

    @Override
    public void sendMessage(String json) throws ConnectionException {

        checkSuccess();

        StringBuilder builder = new StringBuilder();

        builder.append(urlStr)
                .append(URL_ADD_MESSAGE);

        sendJson(builder.toString(), json);
    }

    @Override
    public String getMessages(int lastMessageNumber) {

        try {
            checkSuccess();
        } catch (ConnectionException e) {
            System.err.println(MESSAGE_VALIDATE_SERVER);
        }

        StringBuilder builder = new StringBuilder();

        Map<String, String> params = new HashMap<>();
        params.put("lastMessageNumber", String.valueOf(lastMessageNumber));

        builder.append(urlStr)
                .append(URL_GET_MESSAGES)
                .append(getParamString(params));

        HttpGet request = new HttpGet(builder.toString());
        try {
            HttpResponse response = http.execute(request);

            return getAnswer(response);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(ERROR_GET_MESSAGES);
            return null;
        }

    }

    @Override
    public boolean checkConnection() {

        init();
        HttpGet request = new HttpGet(urlStr);

        try {
            http.execute(request);
            Server.setSuccessConnection(true);
        } catch (IOException e) {
            System.err.println(e);
            return false;
        }
        return true;
    }

    @Override
    public String getOnlineUsers() throws ConnectionException {

        checkSuccess();

        return runSimpleCommand(COMMAND_ONLINE, null);
    }


    @Override
    public String getRooms() throws ConnectionException {

        checkSuccess();

        return runSimpleCommand(COMMAND_CHATROOMS, null);
    }

    @Override
    public String getUsersInRoom(String roomName) throws ConnectionException {
        checkSuccess();

        return runSimpleCommand(COMMAND_GET_USERS, roomName);
    }

    private String runSimpleCommand(String name, String value) {
        StringBuilder builder = new StringBuilder();

        Map<String, String> params = new HashMap<>();
        params.put("name", name);

        if (value != null) {
            params.put("value", value);
        }

        builder.append(urlStr)
                .append(URL_COMMAND)
                .append(getParamString(params));

        HttpGet request = new HttpGet(builder.toString());
        try {
            HttpResponse response = http.execute(request);

            return getAnswer(response);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String sendJson(String url, String json) {
        HttpPost request = new HttpPost(url);

        try {
            StringEntity postString = new StringEntity(json);
            request.setEntity(postString);
            request.setHeader("Content-type", "application/json");
            HttpResponse response = http.execute(request);
            return getAnswer(response);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getParamString(Map<String, String> params) {
        StringBuilder builder = new StringBuilder();

        builder.append("?");

        params.entrySet().stream().forEach(entry -> builder.append(entry.getKey())
                .append("=")
                .append(entry.getValue())
                .append("&"));

        String paramsStr = builder.toString();
        return paramsStr.substring(0, paramsStr.length() - 1);
    }

    private static String getAnswer(HttpResponse response) {

        InputStream is = null;
        try {
            is = response.getEntity().getContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String responseStr = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
        return responseStr;
    }

    private static void doDefaultUser() {
        User user = User.getInstanse();
        user.setAge(null)
                .setEmail(null)
                .setFirstName(null)
                .setNickName(DEFAULT_USER)
                .setSex(null);
    }

    private static void checkSuccess() throws ConnectionException {
        if (!Server.isSuccessConnection()) {
            throw new ConnectionException();
        }
    }
}
