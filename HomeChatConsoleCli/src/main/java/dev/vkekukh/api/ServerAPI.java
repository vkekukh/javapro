package dev.vkekukh.api;


import dev.vkekukh.utils.ConnectionException;

import java.util.HashMap;

public interface ServerAPI {

    void startGettingMessages();

    void sendCommand(HashMap<String, String> params) throws ConnectionException;

    void registerUser(String json) throws ConnectionException;

    void sendMessage(String json) throws ConnectionException;

    void init();

    String getMessages(int lastMessageNumber);

    boolean checkConnection();

    String getOnlineUsers() throws ConnectionException;

    String getRooms() throws ConnectionException;

    String getUsersInRoom(String roomName) throws ConnectionException;

    void doLogin(String login, String password) throws ConnectionException;
}
