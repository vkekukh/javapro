package dev.vkekukh.api;

import dev.vkekukh.utils.ConnectionException;

public interface CommandAPI {
    void doConf() throws ConnectionException;

    void doCommandList() throws ConnectionException;

    void doRegister() throws ConnectionException;

    void doOnline() throws ConnectionException;

    void doCreateChatRoom(String name) throws ConnectionException;

    void doChatRooms() throws ConnectionException;

    void doJoin(String name) throws ConnectionException;

    void doGetUsers(String name) throws ConnectionException;

    void doLogin() throws ConnectionException;

    void doLeave(String value) throws ConnectionException;
}
