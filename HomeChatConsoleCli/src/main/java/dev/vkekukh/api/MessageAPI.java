package dev.vkekukh.api;

import dev.vkekukh.entity.Message;
import dev.vkekukh.utils.ConnectionException;

public interface MessageAPI {
    void sendMessage(Message message) throws ConnectionException;
}
