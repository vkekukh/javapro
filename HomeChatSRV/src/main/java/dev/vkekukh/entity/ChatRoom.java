package dev.vkekukh.entity;

import java.util.ArrayList;
import java.util.List;

public class ChatRoom {

    private User owner;
    private List<User> users = new ArrayList<>();

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public synchronized void addUser(User user) {
        users.add(user);
    }
}
