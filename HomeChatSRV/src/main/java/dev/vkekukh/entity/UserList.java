package dev.vkekukh.entity;

import dev.vkekukh.utils.OnlineMonitor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


import static dev.vkekukh.utils.Const.MAX_INACTIVE_INTERVAL;

public class UserList {
    private final static Logger log = Logger.getLogger(UserList.class);
    private static Set<User> userList = new HashSet<>();
    private static Map<User, HttpSession> onlineMap = new ConcurrentHashMap<>();

    private UserList() {
    }

    static {
        log.debug("Start OnlineMonitor job");
        Thread thread = new Thread(new OnlineMonitor());
        thread.setDaemon(true);
        thread.start();
    }

    public static Set<User> getUserList() {
        return userList;
    }

    public static boolean addUser(User user) {
        if (userList.contains(user)) {
            return false;
        } else {
            userList.add(user);
            return true;
        }
    }

    public static synchronized void addOnlineUser(User user, HttpSession session) {
        session.setMaxInactiveInterval(MAX_INACTIVE_INTERVAL);
        onlineMap.put(user, session);
    }

    public static void removeOnlineUser(User user) {
        onlineMap.remove(user);
    }

    public static Map<User, HttpSession> getOnlineUserMap() {
        return onlineMap;
    }
}
