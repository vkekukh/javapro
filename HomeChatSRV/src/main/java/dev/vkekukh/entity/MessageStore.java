package dev.vkekukh.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class MessageStore {
    private final static Logger log = Logger.getLogger(MessageStore.class);
    private static AtomicInteger index = new AtomicInteger(1);
    private static Map<Integer, Message> messageStore = new ConcurrentHashMap<>();

    public static void addMessage(Message message) {
        log.debug("Add new message to store:" + message);
        message.setId(index.get());
        log.debug(message);
        messageStore.put(index.getAndIncrement(), message);
    }

    public static List<Message> getNewMessages(int startPoint, String nickName) {

        List<Message> newMessages = messageStore
                .entrySet()
                .stream()
                .filter(entry ->
                        entry.getKey() > startPoint
                                && checkVisible(entry.getValue().getTo(), nickName))
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
        return newMessages;
    }

    public static String getJsonMesssages(int startPoint, String nickName) {
        List<Message> list = getNewMessages(startPoint, nickName);
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        return gson.toJson(list);
    }


    private static boolean isNotPrivate(String to, String nickName) {
        return to == null || to.equalsIgnoreCase(nickName);
    }

    private static boolean isToChatRoom(String to, String nickName) {

        ChatRoom room = ChatRooms.getRooms().get(to);
        if (room == null) {
            return false;
        }

        return room.getUsers()
                .stream()
                .filter(entry -> entry.getNickName().equalsIgnoreCase(nickName))
                .count() > 0;
    }

    private static boolean checkVisible(String to, String nickName) {
        if (isNotPrivate(to, nickName)) {
            return true;
        }
        if (isToChatRoom(to, nickName)) {
            return true;
        }
        return false;
    }
}
