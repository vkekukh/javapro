package dev.vkekukh.entity;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChatRooms {
    private static Map<String, ChatRoom> rooms = new ConcurrentHashMap<>();

    public static boolean addChatRoom(String name, ChatRoom room) {
        if (!validate(name)) {
            rooms.put(name, room);
            return true;
        }
        return false;
    }

    private static boolean validate(String name) {
        return rooms.containsKey(name);
    }

    public static Map<String, ChatRoom> getRooms() {
        return rooms;
    }

    public static List<User> getUsersInRoom(String roomName) {
        return rooms.get(roomName.substring(1)).getUsers();
    }

    public static void setRooms(Map<String, ChatRoom> rooms) {
        ChatRooms.rooms = rooms;
    }
}
