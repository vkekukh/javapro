package dev.vkekukh.utils;

public class Const {

    public static final String COMMAND_LIST = "/commandList";
    public static final String COMMAND_ONLINE = "/online";
    public static final String COMMAND_ONLINE_DESC = ": show online users";
    public static final String COMMAND_LIST_DESC = ": list of all commands";
    public static final String COMMAND_REGISTER = "/register";
    public static final String COMMAND_REGISTER_DESC = ": registration in chat";
    public static final String COMMAND_CHAT_MESSAGE = "@<nickName>";
    public static final String COMMAND_CHAT_MESSAGE_DESC = " send message to chat room";
    public static final String COMMAND_CREATE_CHATROOM = "/createRoom";
    public static final String COMMAND_CREATE_CHATROOM_DESC = ": create chatRoom";
    public static final String COMMAND_CHATROOMS = "/chatRooms";
    public static final String COMMAND_CHATROOMS_DESC = ": show all chatRooms";
    public static final String COMMAND_JOIN = "/join";
    public static final String COMMAND_JOIN_DESC = "<chatRoom name>: join to chatRoom";
    public static final String COMMAND_GET_USERS = "/getUsers";
    public static final String COMMAND_GET_USERS_DESC = "<chatRoom name>: show users in Room";
    public static final String COMMAND_LOGIN = "/login";
    public static final String COMMAND_LOGIN_DESC = " : login to chat";
    public static final String COMMAND_LEAVE = "/leave";
    public static final String COMMAND_LEAVE_DESC = "<chatRoom name> : leave room";

    public static final String PRIVATE_MESSAGE = "\"<nickName> : send private message";
    public static final String REGISTER_USER_SUCCESS = "Welcome to our chat!";
    public static final String REGISTER_USER_FAILD = "This nickName already exists";
    public static final String ADD_USER_TO_CHATROOM = "User %s was added to chatRoom";

    public static final String TRY_REGISTER_USER = "Try to register user:";
    public static final String USERS_ONLINE = "Users online: \n";

    public static final String ERROR_ROOM_NOT_FOUND = "Room %s not found";
    public static final String CHAT_ROOM_CREATED = "Chat room %s created";
    public static final String CHAT_ROOM_EXISTS = "Chat room %s already exists";
    public static final String REMOVE_USER = "User was removed from %s";
    public static final String DELETE_ROOM = "You were owner chatRoom %s and room will be removed";

    public static final int MAX_INACTIVE_INTERVAL =  60 * 1000;
    public static final int MONITOR_JOB_INTERVAL = 60 * 1000;

    public static final String DEFAULT_USER = "Annonymous";

    public static final String WRONG_USER_NAME = "Wrong user name";
    public static final String WRONG_PASSWORD = "Wrong password";
}
