package dev.vkekukh.utils;

import dev.vkekukh.entity.UserList;
import dev.vkekukh.servlets.CommandServlet;
import org.apache.log4j.Logger;

import static dev.vkekukh.utils.Const.MAX_INACTIVE_INTERVAL;
import static dev.vkekukh.utils.Const.MONITOR_JOB_INTERVAL;

public class OnlineMonitor implements Runnable {
    private final static Logger log = Logger.getLogger(OnlineMonitor.class);

    @Override
    public void run() {
        log.info("Start OnlineMonitor");

        while (true) {
            log.debug("Check users online");

            UserList.getOnlineUserMap()
                    .entrySet()
                    .removeIf(entry -> {
                        if ((entry.getValue().getLastAccessedTime() + MAX_INACTIVE_INTERVAL) < System.currentTimeMillis()) {
                            log.info("User " + entry.getKey().getNickName() + " is inactive");
                            return true;
                        } else {
                            return false;
                        }
                    });
            try {
                Thread.sleep(MONITOR_JOB_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
