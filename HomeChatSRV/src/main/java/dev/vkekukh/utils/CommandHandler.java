package dev.vkekukh.utils;

import dev.vkekukh.api.CommandAPI;
import dev.vkekukh.api.impl.CommandAPIImpl;
import dev.vkekukh.entity.User;

import static dev.vkekukh.utils.Const.*;

public class CommandHandler {
    private static CommandAPI api = new CommandAPIImpl();

    public static String doCommand(String name, String value, User user) {
        String result = "";
        switch (name) {
            case COMMAND_LIST:
                result = api.doCommandList();
                break;
            case COMMAND_ONLINE:
                result = api.doOnline();
                break;
            case COMMAND_CREATE_CHATROOM:
                result = api.doCreateChatRoom(value, user);
                break;
            case COMMAND_CHATROOMS:
                result = api.doChatRooms();
                break;
            case COMMAND_JOIN:
                result = api.doJoin(user, value);
                break;
            case COMMAND_GET_USERS:
                result = api.doGetUsers(value);
                break;
            case COMMAND_LEAVE:
                result = api.doLeave(value, user);
                break;
        }
        return result;
    }
}
