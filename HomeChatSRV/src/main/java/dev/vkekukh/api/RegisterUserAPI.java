package dev.vkekukh.api;

import javax.servlet.http.HttpSession;

public interface RegisterUserAPI {
    String registerUser(String json, HttpSession session);
}
