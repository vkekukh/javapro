package dev.vkekukh.api.impl;

import dev.vkekukh.api.MessageAPI;
import dev.vkekukh.entity.Message;
import dev.vkekukh.entity.MessageStore;

public class MessageAPIImpl implements MessageAPI{

    @Override
    public void addMessageToStore(String json) {
        MessageStore.addMessage(Message.fromJson(json));
    }
}
