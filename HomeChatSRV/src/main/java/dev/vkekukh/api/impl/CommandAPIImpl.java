package dev.vkekukh.api.impl;

import dev.vkekukh.api.CommandAPI;
import dev.vkekukh.entity.ChatRoom;
import dev.vkekukh.entity.ChatRooms;
import dev.vkekukh.entity.User;
import dev.vkekukh.entity.UserList;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static dev.vkekukh.utils.Const.*;

public class CommandAPIImpl implements CommandAPI {
    private final static Logger log = Logger.getLogger(CommandAPIImpl.class);

    @Override
    public String doCommandList() {
        log.debug(COMMAND_LIST);

        StringBuilder builder = new StringBuilder();
        builder.append(COMMAND_LIST).append(COMMAND_LIST_DESC).append(System.lineSeparator())
                .append(COMMAND_ONLINE).append(COMMAND_ONLINE_DESC).append(System.lineSeparator())
                .append(COMMAND_REGISTER).append(COMMAND_REGISTER_DESC).append(System.lineSeparator())
                .append(COMMAND_LOGIN).append(COMMAND_LOGIN_DESC).append(System.lineSeparator())
                .append(PRIVATE_MESSAGE).append(System.lineSeparator())
                .append(COMMAND_CHAT_MESSAGE).append(COMMAND_CHAT_MESSAGE_DESC).append(System.lineSeparator())
                .append(COMMAND_CREATE_CHATROOM).append(COMMAND_CREATE_CHATROOM_DESC).append(System.lineSeparator())
                .append(COMMAND_CHATROOMS).append(COMMAND_CHATROOMS_DESC).append(System.lineSeparator())
                .append(COMMAND_JOIN).append(COMMAND_JOIN_DESC).append(System.lineSeparator())
                .append(COMMAND_LEAVE).append(COMMAND_LEAVE_DESC).append(System.lineSeparator())
                .append(COMMAND_GET_USERS).append(COMMAND_GET_USERS_DESC).append(System.lineSeparator())
        ;
        return builder.toString();
    }

    @Override
    public String doOnline() {
        String result = UserList.getOnlineUserMap()
                .entrySet()
                .stream()
                .map(entry -> entry.getKey().getNickName())
                .collect(Collectors.joining(System.lineSeparator()));
        return USERS_ONLINE + result;
    }

    @Override
    public String doCreateChatRoom(String name, User user) {
        ChatRoom chatRoom = new ChatRoom();
        chatRoom.setOwner(user);
        chatRoom.addUser(user);
        boolean result = ChatRooms.addChatRoom(name, chatRoom);
        return result ? String.format(CHAT_ROOM_CREATED, name) : String.format(CHAT_ROOM_EXISTS, name);
    }

    @Override
    public String doChatRooms() {
        String chatRooms = ChatRooms.getRooms()
                .entrySet()
                .stream()
                .map(entry -> entry.getKey())
                .collect(Collectors.joining(System.lineSeparator()));
        return chatRooms;
    }

    @Override
    public String doJoin(User user, String chatRoomName) {
        ChatRooms.getRooms().get(chatRoomName).addUser(user);
        return String.format(ADD_USER_TO_CHATROOM, user.getNickName());
    }

    @Override
    public String doGetUsers(String chatRoomName) {
        String users = ChatRooms.getUsersInRoom(chatRoomName)
                .stream()
                .map(user -> user.getNickName())
                .collect(Collectors.joining(System.lineSeparator()));
        return users;
    }

    @Override
    public String doLogin(String nickName, String pass) {

        List<User> userList = UserList.getUserList()
                .stream()
                .filter(user -> user.getNickName().equalsIgnoreCase(nickName))
                .collect(Collectors.toList());
        if (userList.size() != 1) {
            return WRONG_USER_NAME;
        }

        User user = userList.get(0);

        if (!user.getPassword().equals(pass)) {
            return WRONG_PASSWORD;
        }

        return user.toJson();
    }

    @Override
    public String doLeave(String value, User user) {
        Map<String, ChatRoom> rooms = ChatRooms.getRooms();
        ChatRoom room = rooms.get(value);
        if (room == null) {
            return String.format(ERROR_ROOM_NOT_FOUND, value);
        }

        if (room.getOwner().equals(user)) {
            ChatRooms.getRooms().remove(value);
            return String.format(DELETE_ROOM, value);
        }

        ChatRooms.getRooms().get(value).getUsers().remove(user);
        return String.format(REMOVE_USER, value);
    }
}
