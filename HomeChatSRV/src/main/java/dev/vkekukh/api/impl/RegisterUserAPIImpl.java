package dev.vkekukh.api.impl;

import dev.vkekukh.api.RegisterUserAPI;
import dev.vkekukh.entity.User;
import dev.vkekukh.entity.UserList;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSession;

import static dev.vkekukh.utils.Const.REGISTER_USER_FAILD;
import static dev.vkekukh.utils.Const.REGISTER_USER_SUCCESS;


public class RegisterUserAPIImpl implements RegisterUserAPI {
    private final static Logger log = Logger.getLogger(UserList.class);

    @Override
    public String registerUser(String json, HttpSession session) {
        User user = User.fromJson(json);
        String resultStr;

        boolean result = UserList.addUser(user);
        if (result) {
            log.debug("User not  exists");
            session.setAttribute("user", user);
            resultStr = REGISTER_USER_SUCCESS;
        } else {
            log.debug("User " + user.getNickName() + " already exists");
            resultStr = REGISTER_USER_FAILD;
        }
        return resultStr;
    }
}
