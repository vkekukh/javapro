package dev.vkekukh.api;

import dev.vkekukh.entity.User;

public interface CommandAPI {
    String doCommandList();

    String doOnline();

    String doCreateChatRoom(String name, User user);

    String doChatRooms();

    String doJoin(User user, String chatRoomName);

    String doGetUsers(String chatRoomName);

    String doLogin (String nickName, String pass);

    String doLeave(String value, User user);
}
