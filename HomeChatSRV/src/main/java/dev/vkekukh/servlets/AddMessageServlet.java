package dev.vkekukh.servlets;

import dev.vkekukh.api.MessageAPI;
import dev.vkekukh.api.impl.MessageAPIImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@WebServlet("/addMessage")
public class AddMessageServlet extends HttpServlet {
    MessageAPI api = new MessageAPIImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InputStream is = req.getInputStream();
        String json = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
        api.addMessageToStore(json);
    }
}
