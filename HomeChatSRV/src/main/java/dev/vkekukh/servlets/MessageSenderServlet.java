package dev.vkekukh.servlets;

import dev.vkekukh.entity.MessageStore;
import dev.vkekukh.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static dev.vkekukh.utils.Const.DEFAULT_USER;

@WebServlet("/getMessages")
public class MessageSenderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer lastMessageNumber = Integer.valueOf(req.getParameter("lastMessageNumber"));
        User user = (User) req.getSession().getAttribute("user");
        String nickName = (user == null) ? DEFAULT_USER : user.getNickName();
        resp.getWriter().println(MessageStore.getJsonMesssages(lastMessageNumber, nickName));
    }
}
