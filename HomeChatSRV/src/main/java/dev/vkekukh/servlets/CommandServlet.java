package dev.vkekukh.servlets;

import dev.vkekukh.entity.User;
import dev.vkekukh.utils.CommandHandler;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/command")
public class CommandServlet extends HttpServlet {
    private final static Logger log = Logger.getLogger(CommandServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{

        String commandName = req.getParameter("name");
        String commandValue = req.getParameter("value");
        HttpSession session = req.getSession();
        User user =(User) session.getAttribute("user");

        String response = CommandHandler.doCommand(commandName, commandValue, user);

        req.getSession();
        resp.getWriter().println(response);
    }
}
