package dev.vkekukh.servlets;

import dev.vkekukh.api.CommandAPI;
import dev.vkekukh.api.impl.CommandAPIImpl;
import dev.vkekukh.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static dev.vkekukh.utils.Const.WRONG_PASSWORD;
import static dev.vkekukh.utils.Const.WRONG_USER_NAME;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private final static Logger log = Logger.getLogger(CommandAPIImpl.class);
    private CommandAPI api = new CommandAPIImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");
        String pass = req.getParameter("password");

        HttpSession session = req.getSession();
        String result = api.doLogin(login, pass);
        if (result.equals(WRONG_USER_NAME) ||

                result.equals(WRONG_PASSWORD)) {

        } else {
            session.setAttribute("user", User.fromJson(result));
        }
        resp.getWriter().println(result);
    }
}
