package dev.vkekukh.servlets;

import dev.vkekukh.api.RegisterUserAPI;
import dev.vkekukh.api.impl.RegisterUserAPIImpl;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static dev.vkekukh.utils.Const.*;


@WebServlet("/register")
public class RegisterUserServlet extends HttpServlet {
    private final static Logger log = Logger.getLogger(RegisterUserServlet.class);
    RegisterUserAPI api = new RegisterUserAPIImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        InputStream is = req.getInputStream();
        String json = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
        HttpSession session = req.getSession();

        log.info(TRY_REGISTER_USER);
        String result = api.registerUser(json, session);

        resp.getWriter().println(result);
    }
}
